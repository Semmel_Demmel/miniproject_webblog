package de.academy.diedreierdnuesse.comment;

import de.academy.diedreierdnuesse.user.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface CommentRepository extends JpaRepository<Comment, Long> {

    Comment findCommentById(Long id);

    Optional<List<Comment>> findCommentByUser(User user);




}
