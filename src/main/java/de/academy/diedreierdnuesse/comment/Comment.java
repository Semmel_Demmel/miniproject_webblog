package de.academy.diedreierdnuesse.comment;

import de.academy.diedreierdnuesse.user.User;

import javax.persistence.*;
import java.time.Instant;

@Entity
public class Comment {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private long id;

    private String text;
    private Instant creationDateTime;

    @ManyToOne
    private User user;

    public Comment(){}

    public Comment(String text, User user) {
        this.text = text;
        this.user = user;
        this.creationDateTime = Instant.now();
    }


    public String getText() {
        return text;
    }

    public Instant getCreationDateTime() {
        return creationDateTime;
    }

    public long getId() {
        return id;
    }

    public User getUser() {
        return user;
    }
}
