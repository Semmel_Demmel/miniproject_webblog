package de.academy.diedreierdnuesse.session;

import de.academy.diedreierdnuesse.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.util.Optional;

@ControllerAdvice
public class SessionControllerAdvice {

    @Autowired
    private SessionRepository sessionRepository;

    @ModelAttribute("currentUser")
    public User currentUser(@CookieValue(value = "sessionId", defaultValue = "") String sessionId){

        if (sessionId.length() > 0){
            Optional<Session> optionalSession = sessionRepository.findById(sessionId);

            if(optionalSession.isPresent()){
                return optionalSession.get().getUser();
            }
        }
        return null;
    }
}
