package de.academy.diedreierdnuesse.post;

import de.academy.diedreierdnuesse.comment.Comment;
import de.academy.diedreierdnuesse.user.User;

import javax.persistence.*;
import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Post {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private long id;

    private String title;
    private String text;
    private Instant creationDateTime;
    private String imagePath;

    @OneToMany
    @JoinColumn(name = "post_id")
    private List<Comment> comment = new ArrayList<>();

    @ManyToOne
    private User user;


    public Post() {}

    public Post(String title, String text, User user, String imagePath) {
        this.title = title;
        this.text = text;
        this.creationDateTime = Instant.now();
        this.user = user;
        this.imagePath = imagePath;
    }

    public long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getText() {
        return text;
    }

    public Instant getCreationDateTime() { return creationDateTime;
    }

    public List<Comment> getComment() {
        return comment;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public User getUser() {
        return user;
    }
}
