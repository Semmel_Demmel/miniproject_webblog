package de.academy.diedreierdnuesse.login;

import de.academy.diedreierdnuesse.session.Session;
import de.academy.diedreierdnuesse.session.SessionRepository;
import de.academy.diedreierdnuesse.user.User;
import de.academy.diedreierdnuesse.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.Optional;

@Controller
public class loginController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SessionRepository sessionRepository;

    @GetMapping("login")
    public String login(Model model){
        model.addAttribute("login", new LoginDTO());
        return "login";
    }

    @PostMapping("login")
    public String loginSubmit(Model model, @ModelAttribute(value = "login") @Valid LoginDTO loginDTO, BindingResult bindingResult, HttpServletResponse response){

        Optional<User> optionalUser = userRepository.findFirstByUsernameAndPassword(loginDTO.getUsername(), loginDTO.getPassword());

        if(!optionalUser.isPresent()){
            bindingResult.addError(new FieldError("login", "username", "Dieser Benutzername existiert nicht\n oder das Password ist falsch"));
        }


        if(bindingResult.hasErrors()){
            model.addAttribute("login", loginDTO);
            return "login";
        }

        if(optionalUser.isPresent()){
            Session session = new Session(optionalUser.get());
            sessionRepository.save(session);
            Cookie cookie = new Cookie("sessionId", session.getId());
            cookie.setMaxAge(3600);
            response.addCookie(cookie);
            return "redirect:/";
        }
        model.addAttribute("login", loginDTO);
        return "login";
    }

    @PostMapping("logout")
    public String logout(@CookieValue(value = "sessionId", defaultValue = "") String sessionId, HttpServletResponse response){

        sessionRepository.deleteById(sessionId);
        Cookie cookie = new Cookie("sessionId", "");
        cookie.setMaxAge(0);
        response.addCookie(cookie);
        return "redirect:/";

    }






}
