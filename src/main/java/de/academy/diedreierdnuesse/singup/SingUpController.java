package de.academy.diedreierdnuesse.singup;

import de.academy.diedreierdnuesse.user.User;
import de.academy.diedreierdnuesse.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class SingUpController {

    @Autowired
    private UserRepository userRepository;


    @GetMapping("/signup")
    public String signup(Model model){
        model.addAttribute("signup", new SingUpDTO());
        return "signup";
    }

    @PostMapping("signup")
    public String signup(@ModelAttribute("signup") @Valid SingUpDTO signup, BindingResult bindingResult) {
        if (!signup.getPassword1().equals(signup.getPassword2())) {
            bindingResult.addError(new FieldError("signup", "password2", "Passwörter stimmen nicht überein"));
        }

        if (userRepository.existsByUsername(signup.getUsername())) {
            bindingResult.addError(new FieldError("signup", "username", "Benutzername existiert bereits"));
        }

        if (bindingResult.hasErrors()) {
            return "signup";
        }

        User user = new User(signup.getUsername(), signup.getPassword1());
        userRepository.save(user);
        return "redirect:/login";
    }
}
